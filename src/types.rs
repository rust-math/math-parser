//! The types: Provides a sort of type system for the weak-typed AST

use crate::{
    parser::{Node, NodeType, Types},
    tokenizer::{Operation, Token}
};
use math_types::{BigInt, Fraction};
use num_traits::*;
use rowan::WalkEvent;

macro_rules! nth {
    ($self:expr; $index:expr) => {
        $self.children().nth($index).expect("invalid or erroneous ast")
    }
}

#[derive(Clone, Copy, Debug, Fail)]
#[fail(display = "failed to parse float as fraction")]
pub struct ParseFractionError;

#[derive(Clone, Copy, Debug, Fail)]
#[fail(display = "failed to parse int")]
pub struct ParseIntError;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum UnaryOperation {
    BitNot,
    Negate
}

pub trait TypedNode<R: rowan::TreeRoot<Types>>: Sized {
    fn cast(node: Node<R>) -> Option<Self>;
    fn node(&self) -> &Node<R>;
    fn into_node(self) -> Node<R>;
    fn children<'a>(&'a self) -> Box<Iterator<Item = Node<R>> + 'a> where R: 'a {
        Box::new(self.node().children().filter(|node| !node.kind().is_trivia()))
    }
    fn errors<'a>(&'a self) -> Vec<Node<rowan::RefRoot<'a, Types>>> where R: 'a {
        let mut errors = Vec::new();

        for event in self.node().borrowed().preorder() {
            if let WalkEvent::Enter(node) = event {
                if node.kind() == NodeType::Error || node.kind() == NodeType::Token(Token::Error) {
                    errors.push(node);
                }
            }
        }

        errors
    }
}
pub trait UnaryNode<R: rowan::TreeRoot<Types>>: TypedNode<R> {
    fn value(&self) -> Node<R> {
        nth!(self; 0)
    }
}
pub trait TwoOperation<R: rowan::TreeRoot<Types>>: TypedNode<R> {
    fn left(&self) -> Node<R>;
    fn operation(&self) -> Operation;
    fn right(&self) -> Node<R>;
}
macro_rules! typed_node {
    ($($type:expr => $name:ident $(: $trait:ident { $($trait_body:tt)* })* $(: { $($body:tt)* }),*),*) => {
        $(
        #[derive(Clone)]
        pub struct $name<R: rowan::TreeRoot<Types> = rowan::OwnedRoot<Types>>(Node<R>);
        impl<'a> Copy for $name<rowan::RefRoot<'a, Types>> {}
        impl<R: rowan::TreeRoot<Types>> TypedNode<R> for $name<R> {
            fn cast(node: Node<R>) -> Option<Self> {
                if node.kind() == $type {
                    Some($name(node))
                } else {
                    None
                }
            }
            fn node(&self) -> &Node<R> {
                &self.0
            }
            fn into_node(self) -> Node<R> {
                self.0
            }
        }
        $(impl<R: rowan::TreeRoot<Types>> $trait<R> for $name<R> { $($trait_body)* })*
        $(impl<R: rowan::TreeRoot<Types>> $name<R> { $($body)* })*
        )*
    }
}
typed_node! {
    NodeType::Factorial => Factorial: UnaryNode {},
    NodeType::Paren => Paren: UnaryNode {
        fn value(&self) -> Node<R> {
            nth!(self; 1)
        }
    },
    NodeType::Root => Root: UnaryNode {},
    NodeType::Unary => Unary: UnaryNode {
        fn value(&self) -> Node<R> {
            nth!(self; 1)
        }
    }: {
        pub fn operation(&self) -> UnaryOperation {
            match nth!(self; 0).kind() {
                NodeType::Token(Token::BitNot) => UnaryOperation::BitNot,
                NodeType::Token(Token::Operation(Operation::Sub)) => UnaryOperation::Negate,
                _ => panic!("invalid ast")
            }
        }
    },
    NodeType::Token(Token::Ident) => Ident: {
        pub fn as_str(&self) -> &str {
            self.node().borrowed().leaf_text().expect("invalid ast")
        }
    }
}

#[derive(Clone)]
pub struct Value<R: rowan::TreeRoot<Types> = rowan::OwnedRoot<Types>>(Node<R>);
impl<'a> Copy for Value<rowan::RefRoot<'a, Types>> {}
impl<R: rowan::TreeRoot<Types>> TypedNode<R> for Value<R> {
    fn cast(node: Node<R>) -> Option<Self> {
        match node.kind() {
            NodeType::Token(Token::Integer)
            | NodeType::Token(Token::Float) => Some(Value(node)),
            _ => None
        }
    }
    fn node(&self) -> &Node<R> {
        &self.0
    }
    fn into_node(self) -> Node<R> {
        self.0
    }
}
impl<R: rowan::TreeRoot<Types>> Value<R> {
    fn radix_for(mut text: &str) -> (&str, u32) {
        let mut radix = 10;
        if text.chars().nth(0) == Some('0') {
            match text.chars().nth(1) {
                Some('b') => radix = 2,
                Some('o') => radix = 8,
                Some('x') => radix = 16,
                _ => ()
            }
            if radix != 10 {
                text = &text[2..];
            }
        }
        (text, radix)
    }
    pub fn to_bigint(&self) -> Option<Result<BigInt, ParseIntError>> {
        if self.node().kind() == NodeType::Token(Token::Integer) {
            let text = self.node().borrowed().leaf_text().expect("invalid ast");
            let (text, radix) = Self::radix_for(text);
            Some(BigInt::from_str_radix(text, radix).map_err(|()| ParseIntError))
        } else {
            None
        }
    }
    pub fn to_bigfraction(&self) -> Option<Result<Fraction<BigInt>, ParseFractionError>> {
        if self.node().kind() == NodeType::Token(Token::Float) {
            let text = self.node().borrowed().leaf_text().expect("invalid ast");
            let (text, radix) = Self::radix_for(text);
            Some(Fraction::from_str_radix(text, radix).map_err(|()| ParseFractionError))
        } else {
            None
        }
    }
    pub fn as_bigint(&self) -> Option<BigInt> {
        self.to_bigint().map(|r| r.ok())
            .or_else(|| self.to_bigfraction().map(|r| r.ok().map(|f| f.floor())))
            .and_then(|inner| inner)
    }
    pub fn as_bigfraction(&self) -> Option<Fraction<BigInt>> {
        self.to_bigfraction().map(|r| r.ok())
            .or_else(|| self.to_bigint().map(|r| r.ok().map(Fraction::from)))
            .and_then(|inner| inner)
    }
    pub fn to_int(&self) -> Option<Result<i64, std::num::ParseIntError>> {
        if self.node().kind() == NodeType::Token(Token::Integer) {
            let text = self.node().borrowed().leaf_text().expect("invalid ast");
            let (text, radix) = Self::radix_for(text);
            Some(i64::from_str_radix(text, radix))
        } else {
            None
        }
    }
    pub fn to_fraction(&self) -> Option<Result<Fraction<i64>, ParseFractionError>> {
        if self.node().kind() == NodeType::Token(Token::Float) {
            let text = self.node().borrowed().leaf_text().expect("invalid ast");
            let (text, radix) = Self::radix_for(text);
            Some(Fraction::from_str_radix(text, radix).map_err(|()| ParseFractionError))
        } else {
            None
        }
    }
    pub fn as_int(&self) -> Option<i64> {
        self.to_int().map(|r| r.ok())
            .or_else(|| self.to_fraction().map(|r| r.ok().map(|f| f.floor())))
            .and_then(|inner| inner)
    }
    pub fn as_fraction(&self) -> Option<Fraction<i64>> {
        self.to_fraction().map(|r| r.ok())
            .or_else(|| self.to_int().map(|r| r.ok().map(Fraction::from)))
            .and_then(|inner| inner)
    }
}

#[derive(Clone)]
pub struct OperationNode<R: rowan::TreeRoot<Types> = rowan::OwnedRoot<Types>>(Node<R>);
impl<'a> Copy for OperationNode<rowan::RefRoot<'a, Types>> {}
impl<R: rowan::TreeRoot<Types>> TypedNode<R> for OperationNode<R> {
    fn cast(node: Node<R>) -> Option<Self> {
        if node.kind() == NodeType::Operation || node.kind() == NodeType::ImplicitMul {
            Some(OperationNode(node))
        } else {
            None
        }
    }
    fn node(&self) -> &Node<R> {
        &self.0
    }
    fn into_node(self) -> Node<R> {
        self.0
    }
}
impl<R: rowan::TreeRoot<Types>> OperationNode<R> {
    pub fn left(&self) -> Node<R> {
        nth!(self; 0)
    }
    pub fn operation(&self) -> Operation {
        if self.node().kind() == NodeType::ImplicitMul {
            Operation::Mul
        } else {
            match nth!(self; 1).kind() {
                NodeType::Token(Token::Operation(op)) => op,
                _ => panic!("invalid or errnous ast")
            }
        }
    }
    pub fn right(&self) -> Node<R> {
        if self.node().kind() == NodeType::ImplicitMul {
            nth!(self; 1)
        } else {
            nth!(self; 2)
        }
    }
}
