#[macro_use]
extern crate failure;

pub mod parser;
pub mod tokenizer;
pub mod types;

use self::{
    parser::{AST, Parser},
    tokenizer::Tokenizer
};

/// Convenience function that both tokenizes and parses an AST
pub fn parse(input: &str) -> AST {
    Parser::new(Tokenizer::new(input)).parse()
}
