//! The parser: Turn tokens into an AST

use crate::{tokenizer::{Operation, Token}, types::{self, TypedNode}};
use std::iter::Peekable;
use rowan::{GreenNodeBuilder, TextRange, SmolStr};

/// An error that occured during parsing
#[derive(Clone, Fail, Debug)]
pub enum Error {
    #[fail(display = "unexpected eof, expected {:?}", _0)]
    ExpectedNotEOF(Token),
    #[fail(display = "unexpected node {:?} at {}", _0, _1)]
    Node(NodeType, TextRange),
    #[fail(display = "unexpected eof")]
    UnexpectedEOF
}
/// A type of node in the AST
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum NodeType {
    Error,
    Factorial,
    ImplicitMul,
    Operation,
    Paren,
    Root,
    Token(Token),
    Unary
}
impl NodeType {
    /// Returns true if this node is trivia and should be ignored, such as whitespace and errors
    pub fn is_trivia(&self) -> bool {
        match *self {
            NodeType::Error => true,
            NodeType::Token(t) => t.is_trivia(),
            _ => false
        }
    }
}

/// Type that teaches rowan about our NodeType enum
pub struct Types;
impl rowan::Types for Types {
    type Kind = NodeType;
    type RootData = Vec<Error>;
}

pub type Node<R = rowan::OwnedRoot<Types>> = rowan::SyntaxNode<Types, R>;

/// A wrapper around the AST that has convenience functions like returning all errors
#[derive(Clone, Debug)]
pub struct AST {
    root: Node
}
impl AST {
    pub fn node(&self) -> &Node {
        &self.root
    }
    pub fn into_node(self) -> Node {
        self.root
    }
    pub fn root(&self) -> types::Root<rowan::RefRoot<Types>> {
        types::Root::cast(self.root.borrowed()).unwrap()
    }
    pub fn errors(&self) -> Vec<Error> {
        let mut errors = Vec::new();
        errors.extend_from_slice(self.root.root_data());
        errors.extend(
            self.root().errors().into_iter()
                .map(|node| Error::Node(node.kind(), node.range()))
        );
        errors
    }
    pub fn to_result(self) -> Result<Self, Error> {
        if let Some(err) = self.errors().into_iter().next() {
            Err(err)
        } else {
            Ok(self)
        }
    }
}

/// This struct does the parsing
pub struct Parser<I: Iterator<Item = (Token, SmolStr)>> {
    iter: Peekable<I>,
    builder: GreenNodeBuilder<Types>,
    errors: Vec<Error>
}
impl<I: Iterator<Item = (Token, SmolStr)>> Parser<I> {
    pub fn new<F>(iter: F) -> Self
        where F: IntoIterator<Item = I::Item, IntoIter = I>
    {
        Self {
            iter: iter.into_iter().peekable(),
            builder: GreenNodeBuilder::new(),
            errors: Vec::new()
        }
    }

    fn peek(&mut self) -> Option<Token> {
        while self.iter.peek().map(|&(t, _)| t.is_trivia()).unwrap_or(false) {
            self.bump();
        }
        self.iter.peek().map(|&(t, _)| t)
    }
    fn bump(&mut self) {
        if let Some((token, string)) = self.iter.next() {
            self.builder.leaf(NodeType::Token(token), string);
        }
    }
    fn expect(&mut self, expected: Token) {
        match self.peek() {
            None => self.errors.push(Error::ExpectedNotEOF(expected)),
            Some(actual) => if expected == actual {
                self.bump();
            } else {
                self.builder.start_internal(NodeType::Error);
                while self.peek().map(|actual| expected != actual).unwrap_or(false) {
                    self.bump();
                }
                self.bump();
                self.builder.finish_internal();
            }
        }
    }
    fn apply(&mut self, next_fn: fn(&mut Self), ops: &[Operation]) {
        let checkpoint = self.builder.checkpoint();
        next_fn(self);
        while self.peek()
                .map(|token| if let Token::Operation(op) = token { ops.contains(&op) } else { false })
                .unwrap_or(false) {
            self.builder.start_internal_at(checkpoint, NodeType::Operation);
            self.bump();
            next_fn(self);
            self.builder.finish_internal();
        }
    }

    fn parse_val(&mut self) {
        match self.peek() {
            Some(Token::Integer) | Some(Token::Float) | Some(Token::Ident) => self.bump(),
            Some(Token::ParenOpen) => {
                self.builder.start_internal(NodeType::Paren);
                self.bump();
                self.parse_expr();
                self.expect(Token::ParenClose);
                self.builder.finish_internal();
            },
            Some(_) => {
                self.builder.start_internal(NodeType::Error);
                self.bump();
                self.builder.finish_internal();
            },
            None => self.errors.push(Error::UnexpectedEOF)
        }
    }
    fn parse_pow(&mut self) {
        self.apply(Self::parse_val, &[Operation::Pow])
    }
    fn parse_factorial(&mut self) {
        let checkpoint = self.builder.checkpoint();
        self.parse_pow();
        while self.peek() == Some(Token::Factorial) {
            self.builder.start_internal_at(checkpoint, NodeType::Factorial);
            self.bump();
            self.builder.finish_internal();
        }
    }
    fn parse_unary(&mut self) {
        match self.peek() {
            Some(Token::BitNot) | Some(Token::Operation(Operation::Sub)) => {
                self.builder.start_internal(NodeType::Unary);
                self.bump();
                self.parse_unary();
                self.builder.finish_internal();
            },
            _ => self.parse_factorial()
        }
    }
    fn parse_bitshift(&mut self) {
        self.apply(Self::parse_unary, &[Operation::BitShiftL, Operation::BitShiftR]);
    }
    fn parse_implicit_mul(&mut self) {
        let checkpoint = self.builder.checkpoint();
        self.parse_bitshift();
        loop {
            match self.peek() {
                Some(Token::Ident) | Some(Token::ParenOpen) => {
                    self.builder.start_internal_at(checkpoint, NodeType::ImplicitMul);
                    self.parse_bitshift();
                    self.builder.finish_internal();
                },
                _ => break
            }
        }
    }
    fn parse_mul(&mut self) {
        self.apply(Self::parse_implicit_mul, &[Operation::Mul, Operation::Div, Operation::Rem]);
    }
    fn parse_add(&mut self) {
        self.apply(Self::parse_mul, &[Operation::Add, Operation::Sub])
    }
    fn parse_bitand(&mut self) {
        self.apply(Self::parse_add, &[Operation::BitAnd])
    }
    fn parse_bitxor(&mut self) {
        self.apply(Self::parse_bitand, &[Operation::BitXor])
    }
    fn parse_bitor(&mut self) {
        self.apply(Self::parse_bitxor, &[Operation::BitOr])
    }
    fn parse_expr(&mut self) {
        // Always point this to the lowest level math function
        self.parse_bitor();
    }
    fn parse_eq(&mut self) {
        self.apply(Self::parse_expr, &[Operation::Equal]);
    }
    pub fn parse(mut self) -> AST {
        self.builder.start_internal(NodeType::Root);
        self.parse_eq();
        if self.peek().is_some() {
            self.builder.start_internal(NodeType::Error);
            while self.peek().is_some() {
                self.bump();
            }
            self.builder.finish_internal();
        }
        self.builder.finish_internal();
        AST {
            root: Node::new(self.builder.finish(), self.errors)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::tokenizer::Tokenizer;
    use super::*;

    use rowan::WalkEvent;
    use std::fmt::Write;

    fn test(input: &str, expected: &str) {
        let ast = Parser::new(Tokenizer::new(input)).parse().to_result().unwrap().into_node();

        let mut indent = 0;
        let mut out = String::new();
        for event in ast.borrowed().preorder() {
            match event {
                WalkEvent::Enter(node) => {
                    writeln!(out, "{:indent$}{:?}", "", node, indent = indent).unwrap();
                    indent += 2;
                },
                WalkEvent::Leave(_) => indent -= 2
            }
        }

        if out != expected {
            eprintln!("--- Expected ---");
            eprintln!("{}", expected);
            eprintln!("--- Gotten ---");
            eprintln!("{}", out);
            panic!("test failed");
        }
    }

    #[test]
    fn basic() {
        test(
            "1 + 2 * 3",
            "\
Root@[0; 9)
  Operation@[0; 9)
    Token(Integer)@[0; 1)
    Token(Whitespace)@[1; 2)
    Token(Operation(Add))@[2; 3)
    Operation@[3; 9)
      Token(Whitespace)@[3; 4)
      Token(Integer)@[4; 5)
      Token(Whitespace)@[5; 6)
      Token(Operation(Mul))@[6; 7)
      Token(Whitespace)@[7; 8)
      Token(Integer)@[8; 9)
"
        );
    }
    #[test]
    fn unary() {
        test(
            "--~1!",
            "\
Root@[0; 5)
  Unary@[0; 5)
    Token(Operation(Sub))@[0; 1)
    Unary@[1; 5)
      Token(Operation(Sub))@[1; 2)
      Unary@[2; 5)
        Token(BitNot)@[2; 3)
        Factorial@[3; 5)
          Token(Integer)@[3; 4)
          Token(Factorial)@[4; 5)
"
        );
    }
    #[test]
    fn pow() {
        test(
            "1 ** 2!",
            "\
Root@[0; 7)
  Factorial@[0; 7)
    Operation@[0; 6)
      Token(Integer)@[0; 1)
      Token(Whitespace)@[1; 2)
      Token(Operation(Pow))@[2; 4)
      Token(Whitespace)@[4; 5)
      Token(Integer)@[5; 6)
    Token(Factorial)@[6; 7)
"
        );
    }
    #[test]
    fn implicit_mul() {
        test(
            "2x(2 + 3)",
            "\
Root@[0; 9)
  ImplicitMul@[0; 9)
    ImplicitMul@[0; 2)
      Token(Integer)@[0; 1)
      Token(Ident)@[1; 2)
    Paren@[2; 9)
      Token(ParenOpen)@[2; 3)
      Operation@[3; 8)
        Token(Integer)@[3; 4)
        Token(Whitespace)@[4; 5)
        Token(Operation(Add))@[5; 6)
        Token(Whitespace)@[6; 7)
        Token(Integer)@[7; 8)
      Token(ParenClose)@[8; 9)
"
        );
        test(
            "-a b / c d**2",
            "\
Root@[0; 13)
  Operation@[0; 13)
    ImplicitMul@[0; 5)
      Unary@[0; 3)
        Token(Operation(Sub))@[0; 1)
        Token(Ident)@[1; 2)
        Token(Whitespace)@[2; 3)
      Token(Ident)@[3; 4)
      Token(Whitespace)@[4; 5)
    Token(Operation(Div))@[5; 6)
    ImplicitMul@[6; 13)
      Token(Whitespace)@[6; 7)
      Token(Ident)@[7; 8)
      Token(Whitespace)@[8; 9)
      Operation@[9; 13)
        Token(Ident)@[9; 10)
        Token(Operation(Pow))@[10; 12)
        Token(Integer)@[12; 13)
"
        );
    }
    #[test]
    fn bitwise() {
        test(
            "5 & 2 + 1 << 3 * 2",
            "\
Root@[0; 18)
  Operation@[0; 18)
    Token(Integer)@[0; 1)
    Token(Whitespace)@[1; 2)
    Token(Operation(BitAnd))@[2; 3)
    Operation@[3; 18)
      Token(Whitespace)@[3; 4)
      Token(Integer)@[4; 5)
      Token(Whitespace)@[5; 6)
      Token(Operation(Add))@[6; 7)
      Operation@[7; 18)
        Operation@[7; 15)
          Token(Whitespace)@[7; 8)
          Token(Integer)@[8; 9)
          Token(Whitespace)@[9; 10)
          Token(Operation(BitShiftL))@[10; 12)
          Token(Whitespace)@[12; 13)
          Token(Integer)@[13; 14)
          Token(Whitespace)@[14; 15)
        Token(Operation(Mul))@[15; 16)
        Token(Whitespace)@[16; 17)
        Token(Integer)@[17; 18)
"
        );
    }
}
