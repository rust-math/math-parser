//! The tokenizer: Turn text input into tokens

use rowan::SmolStr;

/// An operation, such as addition or subtraction
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Operation {
    Equal,
    Add,
    Sub,
    Mul,
    Div,
    Rem,
    Pow,
    BitAnd,
    BitOr,
    BitXor,
    BitShiftL,
    BitShiftR,
}
/// A token, such as an integer or an identifier
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Token {
    Error,
    Whitespace,

    Integer,
    Float,
    Ident,

    ParenOpen,
    ParenClose,

    BitNot,
    Factorial,
    Operation(Operation),
}
impl Token {
    /// Returns true if this token should be skipped over by the parser, for
    /// example if this is a whitespace or error
    pub fn is_trivia(&self) -> bool {
        match *self {
            Token::Error | Token::Whitespace => true,
            _ => false
        }
    }
}

/// The struct that does the tokenizing
pub struct Tokenizer<'a> {
    input: &'a str,
    offset: usize
}
impl<'a> Tokenizer<'a> {
    pub fn new(input: &'a str) -> Self {
        Self {
            input,
            offset: 0
        }
    }
    fn peek(&self) -> Option<char> {
        self.input[self.offset..].chars().next()
    }
    fn bump(&mut self) {
        if let Some(c) = self.peek() {
            self.offset += c.len_utf8();
        }
    }
    fn since(&self, start: usize, token: Token) -> Option<(Token, SmolStr)> {
        Some((token, SmolStr::new(&self.input[start..self.offset])))
    }
}
impl<'a> Iterator for Tokenizer<'a> {
    type Item = (Token, SmolStr);

    fn next(&mut self) -> Option<Self::Item> {
        let start = self.offset;
        while self.peek().map(char::is_whitespace).unwrap_or(false) {
            self.bump();
        }
        if self.offset > start {
            return self.since(start, Token::Whitespace);
        }

        let c = self.peek();
        self.bump();

        match c {
            None => None,
            Some('+') => self.since(start, Token::Operation(Operation::Add)),
            Some('-') => self.since(start, Token::Operation(Operation::Sub)),
            Some('*') if self.peek() == Some('*') => {
                self.bump();
                self.since(start, Token::Operation(Operation::Pow))
            },
            Some('*') => self.since(start, Token::Operation(Operation::Mul)),
            Some('/') => self.since(start, Token::Operation(Operation::Div)),
            Some('%') => self.since(start, Token::Operation(Operation::Rem)),
            Some('&') => self.since(start, Token::Operation(Operation::BitAnd)),
            Some('|') => self.since(start, Token::Operation(Operation::BitOr)),
            Some('^') => self.since(start, Token::Operation(Operation::BitXor)),
            Some('<') if self.peek() == Some('<') => {
                self.bump();
                self.since(start, Token::Operation(Operation::BitShiftL))
            },
            Some('>') if self.peek() == Some('>') => {
                self.bump();
                self.since(start, Token::Operation(Operation::BitShiftR))
            },
            Some('~') => self.since(start, Token::BitNot),
            Some('!') => self.since(start, Token::Factorial),
            Some('=') => self.since(start, Token::Operation(Operation::Equal)),
            Some('(') => self.since(start, Token::ParenOpen),
            Some(')') => self.since(start, Token::ParenClose),
            Some('0'..='9') => {
                let mut radix = 10;
                if c == Some('0') {
                    match self.peek() {
                        Some('b') => { radix = 2; self.bump(); },
                        Some('o') => { radix = 8; self.bump(); },
                        Some('x') => { radix = 16; self.bump(); },
                        _ => ()
                    }
                }
                while self.peek().map(|c| c.is_digit(radix)).unwrap_or(false) {
                    self.bump();
                }
                if self.peek() == Some('.') {
                    self.bump();
                    while self.peek().map(|c| c.is_digit(radix)).unwrap_or(false) {
                        self.bump();
                    }
                    self.since(start, Token::Float)
                } else {
                    self.since(start, Token::Integer)
                }
            }
            Some('a'..='z') | Some('A'..='Z') => {
                loop {
                    match self.peek() {
                        Some('a'..='z') | Some('A'..='Z') | Some('0'..='9') => self.bump(),
                        _ => break
                    }
                }
                self.since(start, Token::Ident)
            },
            Some(_) => self.since(start, Token::Error)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn tokenize(input: &str) -> Vec<(Token, SmolStr)> {
        Tokenizer::new(input).collect()
    }

    #[test]
    fn basic() {
        assert_eq!(
            tokenize("1 * (2 + 3)"),
            vec![
                (Token::Integer, "1".into()),
                (Token::Whitespace, " ".into()),
                (Token::Operation(Operation::Mul), "*".into()),
                (Token::Whitespace, " ".into()),
                (Token::ParenOpen, "(".into()),
                (Token::Integer, "2".into()),
                (Token::Whitespace, " ".into()),
                (Token::Operation(Operation::Add), "+".into()),
                (Token::Whitespace, " ".into()),
                (Token::Integer, "3".into()),
                (Token::ParenClose, ")".into())
            ]
        );
        assert_eq!(
            tokenize("1.5x << 3"),
            vec![
                (Token::Float, "1.5".into()),
                (Token::Ident, "x".into()),
                (Token::Whitespace, " ".into()),
                (Token::Operation(Operation::BitShiftL), "<<".into()),
                (Token::Whitespace, " ".into()),
                (Token::Integer, "3".into())
            ]
        );
    }
}
